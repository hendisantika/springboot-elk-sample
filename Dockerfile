FROM openjdk:8-jdk-alpine
WORKDIR /app
COPY build/libs/springboot-elk-sample-0.0.1.jar ./springboot-elk-sample-0.0.1.jar
COPY entrypoint.sh ./entrypoint.sh