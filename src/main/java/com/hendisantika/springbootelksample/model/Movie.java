package com.hendisantika.springbootelksample.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elk-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/01/20
 * Time: 21.52
 */
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String title;
    private Integer year;
    private String imdbId;
    private String type;
    private String poster;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(String year) {
        this.year = Integer.parseInt(year);
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }
}