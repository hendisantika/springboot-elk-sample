package com.hendisantika.springbootelksample.repository;

import com.hendisantika.springbootelksample.model.Movie;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elk-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 11/01/20
 * Time: 21.53
 */
public interface MovieRepository extends CrudRepository<Movie, Integer> {

}