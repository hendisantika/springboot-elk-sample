package com.hendisantika.springbootelksample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootElkSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootElkSampleApplication.class, args);
    }

}
